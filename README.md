# LeapTribe

* * *
Code for controlling the computer mouse with an [EyeTribe](https://theeyetribe.com/) and [Leap Motion Controller](https://www.leapmotion.com/).

* * *
### Dependencies
* General
    * win32api
    * pymouse
    * pyHook
* EyeTribe
    * [pytribe](https://github.com/sjobeek/pytribe)
