# -*- coding: utf-8 -*-
'''
    An EyeTribe-controlled mouse cursor
    
    Dominic Canare <dom@greenlightgo.org>
'''
import time

import win32api
from pymouse import PyMouse

import pytribe
from selection_detector import *

import keyboard_hook

FRAME_DELAY = 0.03

mouse = PyMouse()
selectionDetector = DwellSelect(.3, 25)

killKeyDetected = False
def keyListener(event):
    global killKeyDetected

    if event.Key == "F8" and event.Alt > 0:
        killKeyDetected = True
        win32api.PostQuitMessage()
    
keyboard_hook.handlers.append(keyListener)
keyboard_hook.listen()

while not killKeyDetected:
    currentTime = time.time()
    
    data = pytribe.query_tracker()
    if data is not None and data['values']['frame']['state'] != 4:
        point = data['values']['frame']['avg']
        point = (int(point['x']), int(point['y']), 0, currentTime)
        
        selectionDetector.addPoint(point)
        for selection in selectionDetector.getSelections():
            print selection
            mouse.click(selection[0], selection[1])
            #mouse.move(selection[0], selection[1])
        
    time.sleep(FRAME_DELAY)
