# -*- coding: utf-8 -*-

# @TODO: Clear lagging click point when mouse moves far
from math import *

from win32api import GetSystemMetrics
from pymouse import PyMouse

import Leap, sys
from Leap import KeyTapGesture, ScreenTapGesture

def distance(a, b):
    return sqrt(pow(b[0]-a[0], 2) + pow(b[1]-a[1], 2))

class Listener(Leap.Listener):
    def __init__(self):
        super(Listener, self).__init__()
        self.screenSize = [ GetSystemMetrics(0), GetSystemMetrics(1) ]
        self.mouse = PyMouse()
        self.lastPoint = None
        
        self.pushPoint = None
        self.potentialPushPoint = None
        self.pushPointCount = 0

        self.keyPoint = None
        self.potentialKeyPoint = None
        self.keyPointCount = 0

    def on_init(self, controller):
        print "Initialized"
        controller.enable_gesture(Leap.Gesture.TYPE_KEY_TAP);
        controller.enable_gesture(Leap.Gesture.TYPE_SCREEN_TAP);

    def on_connect(self, controller):
        print "Connected"

    def on_disconnect(self, controller):
        print "Disconnected"

    def on_exit(self, controller):
        print "Exited"

    def on_frame(self, controller):
        # Get the most recent frame and report some basic information
        frame = controller.frame()

        if not frame.hands.is_empty:
            interactionBox = frame.interaction_box
            def toScreenCoordinates(pos):
                pos = interactionBox.normalize_point(pos)
                return (
                    int(pos.x * self.screenSize[0]),
                    int(self.screenSize[1] - pos.y * self.screenSize[1]),
                    pos.z
                )

            pointable = frame.pointables.frontmost
            coords = toScreenCoordinates(pointable.stabilized_tip_position)
            if self.potentialKeyPoint == None:
                self.potentialKeyPoint = coords
                self.keyPointCount = 0
            else:
                #print self.potentialKeyPoint, "vs", coords
                
                if distance(self.potentialKeyPoint, coords) < 3:
                    self.keyPointCount += 1
                    if self.keyPointCount == 3:
                        self.keyPoint = self.potentialKeyPoint
                        print "key point activated", self.keyPoint
                else:
                    #print "potential key point cleared"
                    self.potentialKeyPoint = None
                    self.keyPointCount = 0
                    #self.keyPoint = None
             #   if coords[2]
            #print pointable.stabilized_tip_position
#            print frame.hands[0].fingers
 #           if len(frame.hands[0].fingers) > 1:
  #              self.mouse.click(coords[0], coords[1])

            if True:
                for gesture in frame.gestures():
                    if gesture.type == Leap.Gesture.TYPE_KEY_TAP:
                        print "KeyTap"
                        if self.keyPoint != None:
                            print "CLICK"
                            self.mouse.click(self.keyPoint[0], self.keyPoint[1])
    #                    keytap = KeyTapGesture(gesture)
    #                    coords = toScreenCoordinates(keytap.position)
                      #  self.mouse.click(coords[0], coords[1])
                        #print "Key Tap id: %d, %s, position: %s, direction: %s" % (gesture.id, self.state_string(gesture.state), keytap.position, keytap.direction)

                    if gesture.type == Leap.Gesture.TYPE_SCREEN_TAP:
                        print "ScreenTap"
     #                   screentap = ScreenTapGesture(gesture)
     #                   coords = toScreenCoordinates(screentap.position)
                        #self.mouse.click(coords[0], coords[1])
                        #if self.pushPoint != None:
                        #    self.mouse.click(self.pushPoint[0], self.pushPoint[1])
                        #    self.pushPoint = None
                        #print "Screen Tap id: %d, %s, position: %s, direction: %s" % (gesture.id, self.state_string(gesture.state), screentap.position, screentap.direction)

            self.mouse.move(coords[0], coords[1])

def main():
    listener = Listener()
    controller = Leap.Controller()
    controller.set_policy_flags(Leap.Controller.POLICY_BACKGROUND_FRAMES)

    # Have the sample listener receive events from the controller
    controller.add_listener(listener)

    # Keep this process running until Enter is pressed
    print "Press Enter to quit..."
    sys.stdin.readline()

    # Remove the sample listener when done
    controller.remove_listener(listener)


if __name__ == "__main__":
    main()
