# -*- coding: utf-8 -*-
'''
    Selection detection algorithms (for gaze and hand-tracking applications
    
    Dominic Canare <dom@greenlightgo.org>
'''

import math
def distance(a, b):
    return math.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2)
    
def pointMean(pointList):
    point = [0, 0, 0]
    for p in pointList:
        for i in range(3):
            point[i] += p[i]    
    
    for i in range(3):
        point[i] /= len(pointList)
        
    return point

'''
    Abstract class for selection detection
'''
class SelectionDetector(object):
    def __init__(self, bufferSize=256):
        self.points = []
        self.selections = []
        self.bufferSize = bufferSize
    
    '''
        @param point (x, y, z, time) tuple
    '''
    def addPoint(self, point):
        self.points.insert(0, point)
        
        if self.bufferSize != None:
            del self.points[self.bufferSize:]
    
    def getSelections(self):
        selections = list(self.selections)
        self.selections = []
        return selections


'''
    Selects points based on a user dwelling on a particular spot for a minimum amount of time.    
'''
class DwellSelect(SelectionDetector):
    def __init__(self, minimumDelayInSeconds, rangeInPixels):
        super(DwellSelect, self).__init__()
        self.minimumDelay = minimumDelayInSeconds
        self.range = rangeInPixels
        
        self.lastDwellPoint = None
        self.inDwell = False
        
    def addPoint(self, point):
        super(DwellSelect, self).addPoint(point)
        
        if len(self.points) < 2: return
        
        if self.inDwell and distance(self.lastDwellPoint, point) > self.range:
            self.inDwell = False
                
        if not self.inDwell:
            for i in range(1, len(self.points)):
                point = self.points[i]
                if distance(self.points[0], point) > self.range:
                    d = distance(self.points[0], point)
                    break
                    
                if self.points[0][3] - point[3] > self.minimumDelay:
                    # find the mean point and add a timestamp to it
                    meanPoint = pointMean(self.points[:i])
                    meanPoint.append(point[3])
                    
                    self.selections.append(meanPoint)
                    self.lastDwellPoint = meanPoint
                    self.inDwell = True
                    break
            
    
